import {createSlice,current} from "@reduxjs/toolkit";

const initialState = {
    product:[]
};


export const productSlice = createSlice({
    name:'productStore',
    initialState:initialState,
    reducers:{
        addProduct:(state ,action ) => {
            state.product.push(action.payload)
        }
    }
})

export const {addProduct} = productSlice.actions
export default productSlice.reducer