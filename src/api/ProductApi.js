
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const productApi = createApi({
    reducerPath: 'productApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'https://pokeapi.co/api/v2/' }),
    endpoints: (builder) => ({
        getAll: builder.query({
            query: (name) => `pokemon/${''}`,
        }),
    }),
})

export const { useGetAllQuery } = productApi
