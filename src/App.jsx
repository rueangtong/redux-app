import { useState } from 'react'
import {BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import React from "react";
import './App.css'
import Header from './components/Header'
import {Provider} from "react-redux";
import store from "./Store";
import Product from "./components/Product/Product";
import Home from "./components/Home/Home";
function App() {
  const [count, setCount] = useState(0)

    return (
    <div>
        <Provider store={store}>
        <Router>
            <Header/>
            <div className='container'>
                <Routes>
                    <Route path='/Home' element={<Home/>} />
                    <Route path='/Product' element={<Product/>} />
                </Routes>
            </div>
        </Router>
        </Provider>
    </div>
  )
}

export default App
