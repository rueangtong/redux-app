import * as Yup from 'yup'
export { default as Product } from './components/Product/Product'

export const productValidator = Yup.object().shape({
    productName: Yup.string().required('กรุณาระบุชื่อสินค้า'),
    productPrice: Yup.string().required('กรุณาระบุราคา'),
    productType: Yup.string().required('กรุณาระบุประเภทสินค้า'),
    expireDate: Yup.string().when('haveExpire', {
        is: 'true',
        then: (expireDate) => expireDate.required('กรุณาระบุวันที่หมดอายุ')
    }),
    })

