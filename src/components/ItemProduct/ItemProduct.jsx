import React from "react";
import './ItemProduct.css'
import {Box} from "@mui/material";
// import {Link} from 'react-router-dom'

function ItemProduct({product}) {
return (
    <div className='item'>
        <Box  mb={3}>
        <div className= 'item-content'>
            <p>  ชื่อสินค้า : { product.productName}</p>
            <p>ราคา : {product.productPrice }</p>
            <p>ประเภท : {product.productType }</p>
            <p>วันที่หมดอายุ : {product.expireDate ? product.expireDate : 'ไม่มี' }</p>
        </div>
        </Box>
    </div>
)
}
export default ItemProduct