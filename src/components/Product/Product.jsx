import React, {useEffect} from "react";
import { useState } from 'react'
import { useFormik, Formik, Form, Field  } from 'formik'
import { Grid,Box,TextField,Autocomplete,Radio,RadioGroup,FormLabel,FormControlLabel,Button  } from '@mui/material'
import {useDispatch} from "react-redux";
import {addProduct} from "../../Reducer";
import {productValidator } from "../../index";
import "./Product.scss";

function Product() {
    const dispatch = useDispatch()
const [count, setCount] = useState(0)
  const productType = [
      {
          id: '1',
          name: 'householdItems',
          description: 'ของใช้ในบ้าน',
      },
      {
          id: '2',
          name: 'outdoorUse',
          description: 'ของใช้นอกบ้าน',
      },
  ];

const formikProduct  = useFormik({
    initialValues: {
        productName: '',
        productPrice: '',
        productType: '',
        haveExpire: 'true',
        expireDate: '',
    },
    validateOnChange: false,

    onSubmit: (values,actions) => {
        dispatch(addProduct(values))
        formikProduct.resetForm()
    },
    validationSchema :productValidator,
})

    return (
        <div>
            <Box  mb={2}>
                <form onSubmit={formikProduct.handleSubmit}>
                <Grid container spacing={2} columns={11}>
                <Grid item sm={11} md={6}>
                    <TextField id="standard-basic"  name={'productName'} label="ชื่อสินค้า"  variant="standard" onChange={formikProduct.handleChange}
                               value={formikProduct.values.productName} />
                    {formikProduct.errors.productName && ( <p className={'error'} > {formikProduct.errors.productName}</p> )}
                </Grid>
                <Grid item sm={11} md={6}>
                    <TextField id="standard-basic" name={'productPrice'} label="ราคา"  type={"number"} variant="standard" onChange={formikProduct.handleChange}
                               value={formikProduct.values.productPrice} />
                    {formikProduct.errors.productPrice && ( <p className={'error'}> {formikProduct.errors.productPrice}</p> )}
                </Grid>
                    <Box width="100%" />
                <Grid item sm={11} md={4}>
                    <Autocomplete
                        options={productType}
                        getOptionLabel={(productType) => productType.description}
                        id = "grid-choose-pesticide"
                        clearOnEscape
                        name={'productType'}
                        onChange = {
                            (event, newValue) => {
                                formikProduct.setValues({
                                    ...formikProduct.values,
                                    productType: newValue.description,
                                })
                            }
                        }
                        renderInput = {(params) => ( <TextField { ...params} label = "ประเภทสินค้า" />)}
                        />
                    {formikProduct.errors.productType && ( <p className={'error'}> {formikProduct.errors.productType}</p> )}
                </Grid>
                    <Box width="100%" />
                    <Grid item sm={11} md={4}>
                        <FormLabel id="demo-controlled-radio-buttons-group">หมดอายุได้หรือไม่</FormLabel>
                        <RadioGroup aria-labelledby="demo-controlled-radio-buttons-group" name="controlled-radio-buttons-group" value={formikProduct.values.haveExpire}
                            onChange={(e) => {
                                formikProduct.setValues({
                                    ...formikProduct.values,
                                    haveExpire: e.target.value,
                                })
                            }}
                            >
                            <FormControlLabel  value="true" control={<Radio />} label="ได้" />
                            <FormControlLabel  value="false" control={<Radio />} label="ไม่ได้" />
                        </RadioGroup>
                    </Grid>
                    {formikProduct.values.haveExpire == 'true' && (<Grid item sm={11} md={4}>
                        <TextField
                            name={'expireDate'}
                            onChange={formikProduct.handleChange}
                            value={formikProduct.values.expireDate}
                            id="expireDate"
                            label="หมดอายุวันที่"
                            type="date"
                            sx={{ width: 220 }}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                            {formikProduct.errors.expireDate && ( <p className={'error'} > {formikProduct.errors.expireDate}</p> )}
                    </Grid>
                    )}
                </Grid>
                    <Button variant="contained" color="success"  type={"submit"}>
                        บันทึกสินค้า
                    </Button>
                </form>
            </Box>
        </div>
    )

}

export default Product