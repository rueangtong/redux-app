import {React, useEffect,useState} from "react";
import ListProduct from '../ListProduct/ListProduct'

function Home() {

    return (
        <div>
            <h1 style={{   margin: "1rem 0" }}> รายการสินค้า </h1>
            <ListProduct />
        </div>
    )

}
export default Home