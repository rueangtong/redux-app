import React from "react";
import './ListProduct.css'
import {useSelector} from "react-redux";
import ItemProduct from './../ItemProduct/ItemProduct'


function ListProduct() {

    const { product } = useSelector((state) => state.product )

    return(
        <div className= 'product-container'>
            {product && product.map((product) => (
                <ItemProduct key={product.productName} product={product}/>))}
        </div>
    )

}
export default ListProduct